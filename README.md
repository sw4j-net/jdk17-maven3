# Introduction

This project creates a docker image with jdk 17 and maven 3 installed.

The image can be used to run maven builds.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk17-maven3
